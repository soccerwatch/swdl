## \[1.11.1\] - 2023-08-28

### Added

- check also for machine user role during authentication

## \[1.11.0\] - 2023-05-16

### Added

- Add option for having multiple credentials in .swdlrc file

### Fixed

- fix aiohttp error

## \[1.10.1\] - 2023-04-28

### Fixed

- Comparing different IntEnum now returns `False`

## \[1.10.0\] - 2023-04-27

### Added

- Add method to delete AI Tags

## \[1.8.3\] - 2023-04-18

### Fixed

- Increase number of retries on failed API calls

## \[1.8.2\] - 2023-03-09

### Added

- Save MatchData arrays in splitted format and recover them when loading

## \[1.8.1\] - 2023-03-08

### Added

- Add unique_timestamps to MatchData

### Fixed

- Fix Pipeline

## \[1.8.0\] - 2023-02-14

### Added

- Add rudimentary MatchData interface
- Add save and load methods for MatchData
- Add rerender method

## \[1.6.5\] - 2023-01-23

### Added

- store only first poi when rendering with higher framerate
- Add hockey segments from master branch

## \[1.6.4\] - 2022-06-07

### Added

- Add hockey segments

## \[1.6.3\] - 2022-05-09

### Added

- Add segment labels to event enum
- Add ice hockey thirds
