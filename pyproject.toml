[build-system]
requires = ["setuptools>=61.2", "versioningit"]
build-backend = "setuptools.build_meta"

[project]
name = "swdl"
description = "Soccerwatch Data Library"
readme = "README.md"
requires-python = ">=3.8"
authors = [
    { name = "Christian Bodenstein", email = "bodenstein@soccerwatch.tv" },
]
dynamic = ["version"]
dependencies = [
    "future",
    "google-cloud-storage",
    "h5py",
    "m3u8",
    "mock",
    "numpy",
    "pandas",
    "pydoc-markdown",
    "requests",
    "simplejson",
    "tables",
    "pytest",
    "requests-mock",
    "aiohttp",
]

[project.optional-dependencies]
dev = [
    "pre-commit",
    "pytest",
]

[project.urls]
"Homepage" = "https://bitbucket.org/soccerwatch/swdl/"

[tool.setuptools]
packages=["swdl"]
script-files=[
    "bin/swdl-login",
    "bin/swdl-match-info",
]
zip-safe = false


[tool.black]
line-length = 88

[tool.pytest.ini_options]
addopts = ["--strict-markers"]
markers = [
    "unit",
    "integration",
    "system",
]
log_level = "INFO"
mock_use_standalone_module = true


[tool.ruff]
line-length = 88
target-version = "py38"

exclude = [
    ".hypothesis",
    ".pytest_cache",
    ".ruff_cache",
    "build",
    "*.egg-info",
    "tests",
]

select = [
    "B",  # flake8-bugbear
    "C",  # flake8-comprehensions
    "D",  # pydocstyle
    "D401",  # First line of docstring should be in imperative mood
    "E",  # pycodestyle errors
    "EXE",  # flake8-executable
    "F",  # pyflakes
    "I",  # isort
    "PL",  # pylint
    "RUF",  # ruff
    "W",  # pycodestyle warnings
]
ignore = [
    "D100",  # Missing docstring in public module
    "D101",  # Missing docstring in public class
    "D102",  # Missing docstring in public method
    "D103",  # Missing docstring in public function
    "D104",  # Missing docstring in public package
    "D105",  # Missing docstring in magic method
    "D106",  # Missing docstring in public nested class
    "D107",  # Missing docstring in __init__
    "E501",  # line too long, handled by black
    "PLR2004", # Magic value used in comparison, consider replacing 2 with a constant variable
    "PLR0913", # Too many arguments in function definition
]

[tool.ruff.lint]
unfixable = ["F401"]  # Do not fix unused imports, it's annoying during development

[tool.ruff.lint.per-file-ignores]
"__init__.py" = ["F401"]

[tool.ruff.pydocstyle]
convention = "google"

[tool.versioningit]
