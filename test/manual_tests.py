import numpy as np
import pytest

from swdl.matches import Match
from swdl.swrest import DataService


@pytest.mark.skip("Just for manual testing")
def test_cp_upload():
    match = Match(0)
    ds = DataService()
    match.data_service = ds
    uploader = match.create_upload_manager(3)
    uploader.start()
    for i in range(25 * 60 * 60 * 2):
        uploader.push_position(i / 25, np.ones(3, np.float32))
    uploader.stop()


@pytest.mark.skip("Just for manual testing")
def test_cp_download():
    match = Match(0)
    ds = DataService()
    match.data_service = ds
    downloader = match.create_position_downloader(3)
    with downloader:
        for i in range(25 * 60 * 60 * 2):
            pos = downloader.get_position(i / 25)
            print(pos)


@pytest.mark.skip("Just for manual testing")
def test_cp_download_all():
    match = Match(0)
    ds = DataService()
    match.data_service = ds
    match.pull_labels(3)
    assert match.labels.positions.shape[0] > 10000
