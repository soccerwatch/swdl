import pathlib

import pytest


@pytest.fixture(scope="function")
def tmp_file(tmp_path: pathlib.Path) -> pathlib.Path:
    return tmp_path / "tmp_file"
