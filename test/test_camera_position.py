from base64 import b64encode

import numpy as np
import pytest
from mock import patch

from swdl.labels import (
    CP_FRAMERATE,
    CP_INTERVAL,
    CPDownloader,
    CPManager,
    aiohttp,
    get_segment_indeces,
    merge_arrays,
    requests,
    round_timestamps,
    storage,
)


def test_upload():
    with patch.object(storage, "Client", autospec=True) as client:
        man = CPManager()
        bucket = "dummy-bucket"
        client_mock = client.return_value
        bucket_mock = client_mock.bucket.return_value
        blob_mock = bucket_mock.blob.return_value

        blob_mock.download_as_string.return_value = "f33513513513513"

        iterations = 100
        for i in range(iterations):
            data = np.zeros([103, 4], np.float32)
            data[:, 0] = np.arange(i * 103, i * 103 + 103).astype(np.float32) / 25
            encoded_data = b64encode(data.tobytes())
            man.upload_data(encoded_data, "12345", 2, bucket)

        client.assert_called_with()
        client_mock.bucket.assert_called_with(bucket)
        blob_mock.upload_from_string.assert_called_with(
            '{"max_index": 41, "framerate": 25, "interval": 10, "dimensions": 4}'
        )


def test_round_timestamp_shape():
    data = np.ones([22, 4], np.float32)
    data = round_timestamps(data)
    assert data.shape == (250, 4)


def test_round_timestamp_type():
    data = np.ones([25, 4], np.float16)
    data = round_timestamps(data)
    assert data.dtype == np.float32


def test_round_timestamp_index():
    data = np.ones([22, 4], np.float32) * 2
    data = round_timestamps(data)
    assert (data[: CP_FRAMERATE * 2, 0] == 0.0).all()
    assert data[CP_FRAMERATE * 2, 0] == 2.0
    assert (data[CP_FRAMERATE * 2 + 1 :, 0] == 0.0).all()


def test_round_timestamp_rounded():
    data = np.ones([22, 4], np.float32) * 2.00000232
    data = round_timestamps(data)
    assert data[CP_FRAMERATE * 2, 0] == 2.0


def test_round_timestamp_begin():
    data = np.ones([5, 4], np.float32) * 9.998
    data[4] *= 0
    data = round_timestamps(data)
    assert data[0, 0] == 10.0
    assert (data[1:] == 0).all()


def test_round_timestamp_large_number():
    scale = 20014.0
    data = np.ones([22, 4], np.float32) * scale
    data = round_timestamps(data)
    array_target = int((scale % CP_INTERVAL) * CP_FRAMERATE)
    assert data[array_target, 0] == scale


def test_get_segment_indices():
    data = np.array(
        [
            [19.879999, 0.1573928, 0.48274004, 0.51642233],
            [19.92, 0.1573928, 0.48274004, 0.51642233],
            [19.959999, 0.1573928, 0.48274004, 0.51642233],
            [20, 0.1573928, 0.48274004, 0.51642233],
            [20.040001, 0.1573928, 0.48274004, 0.51642233],
            [20.08, 0.1573928, 0.48274004, 0.51642233],
            [20.120001, 0.1573928, 0.48274004, 0.51642233],
        ]
    )
    indces = get_segment_indeces(data)
    assert all(indces == [1, 1, 1, 2, 2, 2, 2])


def test_merge_arrays():
    old = np.array([[0, 0, 0, 0], [1, 0, 0, 0], [0, 0, 0, 0]])
    new = np.array([[5, 0, 0, 0], [0, 0, 0, 0], [2, 0, 0, 0]])
    result = np.array([[5, 0, 0, 0], [1, 0, 0, 0], [2, 0, 0, 0]])
    assert (merge_arrays(old, new) == result).all()


def test_cp_downloader_initial_fetch():
    with patch.object(requests, "get", autospec=True) as get_mock, patch.object(
        aiohttp, "ClientSession", autospec=True
    ):
        CPDownloader("0", 0)
        get_mock.assert_called_with(
            "https://storage.googleapis.com/sw-sc-de-shared/0/"
            "camera_positions/0/cp.json"
        )


def test_cp_downloader_check_ctx_manager():
    with patch.object(requests, "get", autospec=True), patch.object(
        aiohttp, "ClientSession", autospec=True
    ):
        cp = CPDownloader("0", 0)
        with cp as ref:
            assert cp == ref


def test_cp_downloader_check_not_started():
    with patch.object(requests, "get", autospec=True), patch.object(
        aiohttp, "ClientSession", autospec=True
    ):
        cd = CPDownloader("0", 0)
        with pytest.raises(RuntimeError):
            cd.get_position(0)


def test_cp_downloader_get_rounded_timestamp_non_zero():
    segment = np.mgrid[:250, :4][0].astype(np.float32) / 25
    poi = CPDownloader._get_rounded_cam_pos(14, segment)
    assert any(poi == 4)
