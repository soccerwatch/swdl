import pytest
from mock import patch

from swdl.config import Settings


def test_set_api_env_default():
    settings = Settings()
    assert (
        settings.DISCOVERY_URL
        == "https://aisw-ww-prod.ew.r.appspot.com/api-discovery/service/prod"
    )


@pytest.mark.parametrize(
    "env, discovery",
    (
        ("prod", "https://aisw-ww-prod.ew.r.appspot.com/api-discovery/service/prod"),
        ("dev", "https://aisw-ww-prod.ew.r.appspot.com/api-discovery/service/dev"),
        ("unknown", "https://aisw-ww-prod.ew.r.appspot.com/api-discovery/service/prod"),
    ),
)
def test_set_api_env(env, discovery):
    def env_return(name, _):
        if name == "SW_CLOUD_API_ENV":
            return env

    with patch("os.environ.get", side_effect=env_return):
        settings = Settings()
        assert settings.DISCOVERY_URL == discovery
