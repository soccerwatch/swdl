import pytest

from swdl.labels import Events, GameStatus


class TestUnequalIntEnum:
    @pytest.mark.parametrize(
        "i", {int(i) for i in GameStatus} & {int(i) for i in Events}
    )
    def test_different_intenums_are_not_equal(self, i):
        assert Events(i) != GameStatus(i)

    def test_different_intenums_are_not_equal_concrete(self):
        assert Events.KICKOFF != GameStatus.FIRST_START

    @pytest.mark.parametrize("i", {int(i) for i in Events})
    def test_same_intenums_are_equal(self, i):
        assert Events(i) == Events(i)

    def test_same_intenums_are_equal_concrete(self):
        assert Events.KICKOFF == Events(1)

    @pytest.mark.parametrize("i", {int(i) for i in Events})
    def test_intenum_is_equal_to_int(self, i):
        assert Events(i) == i

    def test_intenum_is_equal_to_int_concrete(self):
        assert Events.KICKOFF == 1

    def test_intenum_is_not_equal_to_str(self):
        assert Events.KICKOFF != "1"

    def test_intenum_is_not_equal_to_anything_else(self):
        assert Events.KICKOFF != object()
