# coding: utf8
import re
from datetime import datetime

import mock
import pytest
import requests
import simplejson as json

from swdl.camera import Camera
from swdl.matches import FFMPegRunner, Match
from swdl.swrest import AuthService, DataService

test_match = Match(1300)
test_match.match_id = "1300"
test_match.camera_id = "51"
test_match.location = (
    "Kunstrasenplatz, ESO-Stadion Auf der Emst, Am Südenberg 36a, 58644 Iserlohn"
)
test_match.state = "done"
test_match.user_stream_link = (
    "https://storage.googleapis.com/sw-sc-de-shared/1300/720p/1300.m3u8"
)
test_match.grid_stream_link = (
    "https://xfiles100.blob.core.windows.net/1300/Grid/1300.m3u8"
)
test_match.team_name_home = "FC Bor. Dröschede"
test_match.team_name_away = "DJK VfL Billerbeck"
test_match.date = datetime.fromtimestamp(float(1510488900000) / 1000)
test_match.active = True


def assert_equal_match(match1: Match, match2: Match):
    assert match1.team_name_away == match2.team_name_away
    assert match1.team_name_home == match2.team_name_home
    assert match1.camera_id == match2.camera_id
    assert match1.date == match2.date
    assert match1.grid_stream_link == match2.grid_stream_link
    assert match1.user_stream_link == match2.user_stream_link
    assert match1.state == match2.state
    assert match1.location == match2.location
    assert match1.active == match2.active


@pytest.fixture
def data_service():
    with mock.patch("swdl.swrest.AuthService"):
        with mock.patch("swdl.swrest.DataService._get_apis"):
            return DataService()


def test_fetch_valid_single(data_service, requests_mock):
    requests_mock.get("http://test", status_code=200, text='{"data": "test"}')

    it = data_service.fetch("http://test")
    ret = next(iter(it))
    assert ret["data"] == "test"


def test_fetch_server_error(data_service, requests_mock):
    requests_mock.get("http://test", status_code=500, text='{"data": "test"}')

    with mock.patch("time.sleep"):
        with pytest.raises(ConnectionError):
            it = data_service.fetch("http://test")
            list(it)


def test_fetch_login_error(data_service, requests_mock):
    requests_mock.get("http://test", status_code=403, text='{"data": "test"}')

    with mock.patch("time.sleep"):
        with pytest.raises(ConnectionError):
            it = data_service.fetch("http://test")
            list(it)


def test_fetch_valid_multiple(data_service, requests_mock):
    data = [
        '{"data": 1, "nextToken": "a"}',
        '{"data": 2, "nextToken": "b"}',
        '{"data": 3, "nextToken": "c"}',
        '{"data": 4, "nextToken": "d"}',
        '{"data": 5, "nextToken": "e"}',
        '{"data": 6}',
    ]
    matcher = re.compile(r"http://test")

    requests_mock.post(matcher, [{"text": d} for d in data])

    it = data_service.fetch("http://test", body={})
    ret = list(it)
    assert len(ret) == 6
    assert ret[0]["data"] == 1
    assert ret[5]["data"] == 6


def test_fetch_invalid_json(data_service, requests_mock):
    requests_mock.get("http://test", status_code=200, text='{"fake": 2,\n "fake2": 3')

    with mock.patch("time.sleep"):
        it = data_service.fetch("http://test")
        with pytest.raises(ConnectionError):
            next(it)


def test_fetch_invalid_url(data_service, requests_mock):
    requests_mock.get("http://test", status_code=403, text="Invalid URL")

    with mock.patch("time.sleep"):
        it = data_service.fetch("http://test")
        with pytest.raises(ConnectionError):
            next(it)


def test_delete_valid(data_service, requests_mock):
    requests_mock.delete("http://test", status_code=200)

    it = data_service.delete("http://test")
    next(it)
    assert requests_mock._adapter.last_request._request.method == "DELETE"


def test_get_valid_match(data_service):
    with mock.patch.object(DataService, "fetch"):
        with open("test_match_1300.json", encoding="utf-8") as h:
            data_service.fetch.return_value = [json.load(h)]
        m = data_service.get_match(1300)
        assert_equal_match(m, test_match)


def test_get_valid_match_from_string(data_service):
    with mock.patch.object(DataService, "fetch"):
        with open("test_match_1300.json", encoding="utf-8") as h:
            data_service.fetch.return_value = [json.load(h)]
        m = data_service.get_match("1300")
        assert_equal_match(m, test_match)


def test_update_match(data_service):
    with mock.patch.object(DataService, "fetch"):
        with open("test_match_1300.json", encoding="utf-8") as h:
            data = json.load(h)
        data_service.fetch.return_value = [data]
        m = data_service.get_match(1300)
        data["state"] = "running"
        data_service.fetch.return_value = [data]
        m.pull_info()
        assert m.state == "running"


def test_get_matches(data_service):
    with open("test_list_matches.json", encoding="utf-8") as h:
        data = json.load(h)
    with mock.patch.object(data_service, "fetch"):
        data_service.fetch.return_value = [data]
        it = data_service.get_matches()
        m = list(it)
        assert len(m) == 250


def test_update_events(data_service):
    with open("test_events.json", encoding="utf-8") as h:
        data = json.load(h)
    with mock.patch.object(data_service, "get_events"):
        data_service.get_events.return_value = data
        m = test_match
        test_match.data_service = data_service
        m.pull_events()
        num_events = m.labels.events.shape[0]
        assert num_events == 14


def test_find_duration():
    test_string = " Duration: 02:29:56.61, start: 3630.021300, bitrate: 0 kb/s"
    duration = int(FFMPegRunner.find_duration(test_string))
    assert duration == 2 * 60 * 60 + 29 * 60 + 56


def test_find_position():
    test_string = (
        "frame=   50 fps=0.0 q=-1.0 Lsize=     "
        "776kB time=02:02:02.71 bitrate=2345.6kbits/s speed=2.75x "
    )
    position = int(FFMPegRunner.find_position(test_string))
    assert position == 2 * 60 * 60 + 2 * 60 + 2


def test_upload_player_positions(data_service):
    import numpy as np

    players = np.empty((25, 5), np.int32)
    match = test_match
    match.data_service = data_service
    with mock.patch.object(data_service, "fetch"):
        match.add_player_position(100, players)
        data_service.fetch.assert_called_once()


def test_print_match(data_service):
    print(test_match)


test_camera = Camera(
    "310",
    None,
    "idle",
    "K2",
    "Kunstrasenplatz, Sportplatz Merfeld (KR), Rekener Str., 48249 Dülmen",
    "ACTIVE",
)


def assert_camera_equal(cam1: Camera, cam2: Camera):
    assert cam1.id == cam2.id
    assert cam1.address == cam2.address
    assert cam1.system_state == cam2.system_state
    assert cam1.current_task == cam2.current_task


def test_get_camera(data_service):
    with mock.patch.object(data_service, "fetch", autospec=True):
        data_service.fetch.return_value = json.load(
            open("test_camera.json", encoding="utf-8")
        )
        cam = data_service.get_camera(310)
        assert_camera_equal(cam, test_camera)


def test_set_stitching_status(data_service):
    with mock.patch.object(data_service, "fetch", autospec=True) as fetch:
        data_service.set_camera_stitching_status("100", "calibrated")
        fetch.assert_called_with(mock.ANY, {"uid": "100", "stitching": "calibrated"})


class TestAuthService:
    @pytest.fixture(scope="function")
    def response(self):
        return mock.Mock(spec=requests.Response)

    @pytest.fixture(scope="function")
    def sign_in_mock(self, requests_mock):
        yield requests_mock.post(
            "https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword",
            text="""{
                "idToken": "test_id",
                "refreshToken": "test_refresh",
                "expiresIn": 600
            }""",
        )

    @pytest.fixture(scope="function")
    def refresh_mock(self, requests_mock):
        yield requests_mock.post(
            "https://identitytoolkit.googleapis.com/v1/token",
            text="""{
                "id_token": "test_id_refreshed",
                "refresh_token": "test_refresh_refreshed",
                "expires_in": 600
            }""",
        )

    @pytest.fixture(scope="function")
    def auth_service(self, sign_in_mock):
        auth_service = AuthService("test_user", "test_password")
        return auth_service

    def test_auth_service_sign_in(self, auth_service, sign_in_mock):
        assert sign_in_mock.called
        assert auth_service.id_token == "test_id"
        assert auth_service.refresh_token == "test_refresh"

    def test_auth_service_refresh_token(self, auth_service, refresh_mock):
        auth_service._refresh_token()
        assert refresh_mock.called
        assert auth_service.id_token == "test_id_refreshed"
        assert auth_service.refresh_token == "test_refresh_refreshed"

    def test_auth(self, auth_service):
        request = mock.Mock(spec=requests.Request)
        request.headers = {}
        r = auth_service.auth(request)
        assert r.headers["authorization"] == "Bearer test_id"
