import numpy as np
import pandas as pd
import pytest

from swdl.match_data import MatchData


def assert_match_data_equals(md1: MatchData, md2: MatchData):
    pd.testing.assert_frame_equal(md1.objects, md2.objects)
    pd.testing.assert_frame_equal(md1.events, md2.events)
    pd.testing.assert_frame_equal(md1.camera_positions, md2.camera_positions)
    assert md1.match_id == md2.match_id


@pytest.fixture(scope="function")
def filled_match_data():
    match = MatchData("12345")
    match.objects = pd.DataFrame(
        {
            "label": ["player", "ball"],
            "timestamp_ns": [1, 2],
            "top_view_pos": [np.array([0.5, 0.5]), np.array([0.1, 0.1])],
            "match_id": [match.match_id, match.match_id],
        }
    )
    return match


def test_eq(filled_match_data):
    """Test if match_data is equal to itself."""
    assert_match_data_equals(filled_match_data, filled_match_data)


def test_not_eq(filled_match_data):
    """Test if match_data is not equal to empty."""
    with pytest.raises(AssertionError):
        assert_match_data_equals(filled_match_data, MatchData())


def test_save_load_empty(tmp_file):
    """Test save and load with empty MatchData."""
    match = MatchData()
    match.save(tmp_file)
    match_loaded = MatchData.from_file(tmp_file)
    assert_match_data_equals(match, match_loaded)


def test_save_load_filled(filled_match_data, tmp_file):
    """Test save and load with filled match data."""
    filled_match_data.save(tmp_file)
    match_loaded = MatchData.from_file(tmp_file)
    assert_match_data_equals(filled_match_data, match_loaded)


def test_save_twice_load(filled_match_data, tmp_file):
    """Test save twice and load with filled match data."""
    filled_match_data.save(tmp_file)
    filled_match_data.save(tmp_file)
    match_loaded = MatchData.from_file(tmp_file)
    assert_match_data_equals(filled_match_data, match_loaded)


def test_save_load_no_merge(filled_match_data, tmp_file):
    """Test save and load with filled match data."""
    filled_match_data.save(tmp_file)
    match_loaded = MatchData.from_file(tmp_file, merge=False)
    assert all(
        match_loaded.objects.columns
        == ["label", "timestamp_ns", "top_view_pos#0", "top_view_pos#1", "match_id"]
    )


def test_load_invalid(tmp_file):
    """Test load not existing match data file."""
    with pytest.raises(OSError):
        MatchData.from_file(tmp_file)


def test_load_timestamps(filled_match_data, tmp_file):
    """Test load not existing match data file."""
    filled_match_data.save(tmp_file)
    ts = filled_match_data.load_timestamps(tmp_file)
    assert all(ts == pd.DataFrame({"timestamp_ns": [1, 2]}))


def test_insert_object(filled_match_data):
    """Test insert_object will increase the objects dataframe."""
    previous_length = len(filled_match_data.objects)
    df_object = pd.DataFrame({"top_view_pos": [np.ndarray((1, 1))]})
    filled_match_data.insert_objects(df_object)
    assert len(filled_match_data.objects) == previous_length + 1
